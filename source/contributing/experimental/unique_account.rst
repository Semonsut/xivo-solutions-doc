**************
Unique Account
**************

.. warning:: Experimental Feature

    * this is Work In Progress
    * this is not for production use
    * no support will be provided for this feature


Description
===========

The goal of this feature is to be able for a user to use, with the same number, its deskphone or UC Assistant.

Use case:

* Given a user U1 with internal number 1000 and UA activated
* Given U1 line provisionned on a deskphone P1
* Then, when someone calls 1000, P1 rings
* But when U1 logs in UC Assistant then, if someone calls 1000, the call rings on U1's UC Assistant
* When U1 logs out from the UC Assistant then, if someone calls 100, the call rings on U1's deskphone


Work in Progress
================

.. |checked| raw:: html

    <input checked=""  disabled="" type="checkbox">

.. |unchecked| raw:: html

    <input disabled="" type="checkbox">

* |checked| Enable UA on a User
* |checked| Be able to call from the UC Assistant when logged
* |checked| Be able to receive a call on UC Assistant when logged
* |checked| Be able to have history of both my deskphone and webrtc line
* |checked| Have the correct presence shown to other users
* |checked| Be able to check my voicemail from deskphone and UC Assistant
* |checked| Have voicemail notification on deskphone and UC Assistant
* |checked| Have correct handling of No Answer scenario

Forwarding
==========

For a *Unique Account* user, the forwarding rules are applied to the WebRTC user if he's connected to the UC Assistant.

For exemple if the user is connected to the UC Assistant and doesn't answer a call,
the non answer rule is directly applied without make the desktop phone ringing.

Configuration
=============

To enable the *Unique Account* feature for a user:

#. Edit the user
#. Go to the *Lines* tab and click on the line's name
#. It opens the Line menu. In the Line menu, go to the *Advanced* tab
#. Add the option ``webrtc`` with value **ua**
#. Save line

The Unique Account feature is activated for this user.


Connection to UC Assistant
==========================

With a *Unique Account* user you can connect to the UC Assistant.
When connected, the user will be connected as a WebRTC user.

#. Go to https://xivocc/ucassistant
#. Login with CTI credential of UA user
#. The user is logged in with its WebRTC line
#. Now:

  * All calls towards your user's extension will be sent to your UC Assistant
  * You can also place internal our outgoing calls with your UC Assistant

Call history
============

Once you are connected to UC assistant you are able to see mixed history of your 
deskphone and your calls made through webrtc.

.. note:: If someone is making a call with deskphone while you are connected to UC assistant, the call will be also displayed in call history.

Limitations
===========

* If a *Unique Account* user logs in UC Assistant and then quits the page (without logging out),
  **it can take few minutes before the next call will ring on its deskphone**
  (the WebRTC line gets unregistered, but after a delay depending on XiVO configuration).
* In XDS configuration, a *Unique Account* users will not be able to register on media servers but only on Main XiVO so *Unique Account* users should not be created on media servers.
