.. _xivocc_upgrade_proc_deb_9:

***********************************************
XiVO UC/CC Debian 9 (Stretch) Upgrade Procedure
***********************************************

This page describes what to do to upgrade your XiVO UC/CC to Debian 9 (Stretch).

.. important:: Upgrade your XiVO UC/CC to Debian9 (Stretch) is a **mandatory** step when upgrading to Electra because system freezes
               were detected during test of Electra version with kernel 3.16. Problem was solved with
               kernel 4.9 and higher.

.. warning:: In Debian 9 (Stretch) docker storage driver changed from ``aufs`` to ``overlay2``.
   Therefore all containers and images need to be recreated.
   Note that ``overlay2`` is incompatible with **XFS partition created without** ``ftype=1`` **option**.
   If the partition is XFS, you **MUST** check if the option is enabled with the ``xfs_info`` command.


Checks
======

What to check before upgrading to Debian 9 (Stretch):

* your XiVO UC/CC **MUST** be in Deneb version otherwise all the database data **will be lost !**
* the partition where docker data is stored must not be using XFS file system with ftype different from 1.
  You can check it with following command (assuming that docker data dir is ``/var/lib/docker``)::

   xfs_info /var/lib/ 2> /dev/null | grep ftype

When
====

The upgrade to Debian 9 (Stretch) should be done **before** upgrading to Electra (if you are already in Deneb).

It can be done *after* upgrading to Electra, but it should be done *just after*. Otherwise your system won't be stable.

Backups
=======

During upgrade all **Kibana configuration** (including the dashboard) will be lost (they are stored in *elasticsearch* container).

You **MUST** :ref:`backup Kibana configuration <xivocc_backup_kibana>` before the upgrade.

Since Deneb version, the database data was exported to the host.
Therefore no data loss should happen even when removing the pgxivocc container.

Though we **strongly advise** to :ref:`backup the database <xivocc_backup_db>` for safety.


Upgrade
=======

After having backup your configuration you're ready to upgrade the host to Debian 9 (Stretch).
We don't cover the Debian upgrade procedure itself here, this can be found on `Debian 9 release notes <https://www.debian.org/releases/stretch/amd64/release-notes/ch-upgrading.fr.html>`_.

Here we'll only give the main steps and **where specific actions are to be performed in a XiVO UC/CC context**.

The upgrade consist of:

#. Updating the host to latest Debian 8 (Jessie) version,
#. Switching the sources list to Debian 9 (Stretch). **Specifically on a XiVO UC/CC** you must
   also update the sources list of the docker repo:

   .. code-block:: bash

     sed -i 's/jessie/stretch/' /etc/apt/sources.list /etc/apt/sources.list.d/docker.list

#. Launch the Debian 8 to Debian 9 upgrade
#. **Specifically on a XiVO UC/CC** and **before** rebooting you **MUST** remove the ``aufs`` dir:

   .. code-block:: bash

     systemctl stop docker
     rm -rf /var/lib/docker/aufs

#. Reboot the machine
#. **Specifically on a XiVO UC/CC** and **after** the reboot you must pull the images again:

   .. code-block:: bash

     xivocc-dcomp pull
     xivocc-dcomp up -d


If at this last step it doesn't work yet, you should check and remove again the aufs dir:

.. code-block:: bash

  xivocc-dcomp stop
  xivocc-dcomp rm
  systemctl stop docker
  rm -rf /var/lib/docker/aufs
  xivocc-dcomp pull
  xivocc-dcomp up -d


Restore
=======

At least you must :ref:`restore the Kibana configuration <xivocc_restore_kibana>`.

