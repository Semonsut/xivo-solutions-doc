.. _agent_api:

*********
Agent API
*********

This API is dedicated to handle Agents for CC activities.

A sample of implementation is available in `app/assets/javascripts/pages/sampleAgents.js`
and  `app/views/sample/sampleAgents.scala.html`

Login and Authentication
=========================
An agent can be logged in using `Cti.loginAgent(agentPhoneNumber, agentId)`. For the moment, the phone number used for
agent login should be the same as the one used for user login, otherwise you will get many error messages
"LoggedInOnAnotherPhone".

Following cases are handled:

- agent is not logged and requests a login to a known line: the agent is logged in
- agent is not logged and requests a login to an unknown line: an error is raised:

::

    {"Error":"PhoneNumberUnknown"}

- agent is already logged on the requested line: the agent stays logged
- agent is already logged on another line: an error is raised and the agent stays logged (on the number where he was
  logged before the new request). It's up to the implementation to handle this case.

::

    {"Error":"LoggedInOnAnotherPhone","phoneNb":"1002","RequestedNb":"1001"}

- agent is not logged and requests a login to a line already used by another agent: the agent takes over the line and
  the agent previously logged on the line is unlogged

Security considerations
=======================

Defining a profile in the ConfigMGT impact the behavior of this api.

No Profile
----------

If no profile is found, the behavior falls back on the Admin Profile behavior.

Admin Profile
-------------

An admin profile will be allowed to receive all events and send all commands.

Supervisor Profile
------------------

A supervisor profile has the some properties impacting the events he can receive:

* A list of queue which will filter the following events based on the queues in this list (send event only for queues defined in the list):

  * QueueList
  * QueueMemberList
  * QueueStatistics

* A list of groups which will filter the following events based on the groups in this list (send event only if matching agent group is in the list):

  * AgentStateEvent
  * AgentStatistics
  * AgentGroupList
  * AgentList


Agent Methods
=============

Cti.loginAgent(agentPhoneNumber, agentId)
-----------------------------------------
Log an agent

Cti.logoutAgent(agentId)
------------------------
Un log an agent

.. _agent_pause_api:

Cti.pauseAgent(agentId, reason)
-------------------------------
Change agent state to pause.

* `agentId`: Id of the agent to set state for. Can be omitted to change current loggedin agent state.
* `reason`: Optional string to label the kind of pause to set.

Cti.unpauseAgent(agentId)
-------------------------
Change agent state to ready

* `agentId`: Id of the agent to set state for. Can be omitted to change current loggedin agent state.

Cti.listenAgent(agentId)
------------------------
Listen to an agent

Cti.setAgentQueue(agentId, queueId, penalty)
--------------------------------------------
* agentId (Integer) : id of agent, returned in message `Agent Configuration`_
* queueId (Integer) : id of queue, returned in message :ref:`queue_configuration_api`
* penaly (Integer) : positive integer

If agent is not associated to the queue, associates it, otherwise changes the penalty

On success triggers a :ref:`queue_member_api` event, does not send anything in case of failure :

::

    {"agentId":<agentId>,"queueId":<queueId>,"penalty":<penalty>}

Cti.removeAgentFromQueue(agentId, queueId)
------------------------------------------
* agentId (Integer) : id of agent, returned in message `Agent Configuration`_
* queueId (Integer) : id of queue, returned in message :ref:`queue_configuration_api`

On success triggers a queue member event with penalty equals to -1, does not send anything in case of failure :

::

    {"agentId":<agentId>,"queueId":<queueId>,"penalty":-1}


Cti.dialFromQueue(destination, queueId, callerId, variables)
------------------------------------------------------------

Creates outgoing call to ``destination`` from some free Agent attached to ``queueId``. Caller id on both sides is set
to ``callerId``.

Variables must take the following form:

::

    {
        var1: "value 1",
        var2: "value 2"
    }

USR_var1 and USR_var2 will be attached to the call and propagated to :ref:`phoneevents`

**Limitations:** Queue No Answer settings does not work - see :ref:`xivo-queues-no-answer`. Except: when there is no free Agent to queue (none attached, all Agents on pause or busy), then No answer settings work (but Fail does not).

.. note:: Line should be configured with enabled "Ring instead of On-Hold Music" enabled (on "Application: tab in queue configuration - see :ref:`xivo-queues`). Otherwise the queue will answers the call and the destination rings even if there are no agents available.

Cti.monitorPause(agentId)
-------------------------
Pause call recording

.. note:: You can only pause the recording of a call answered by an agent (i.e. a call sent via a Queue towards an Agent).

Cti.monitorUnpause(agentId)
---------------------------
Unpause call recording

.. note:: You can only pause the recording of a call answered by an agent (i.e. a call sent via a Queue towards an Agent).

Cti.subscribeToAgentStats()
---------------------------

Subscribe to agent statistics notification. When called all current statistics are receive, and a notification is received for each updates. Both initial values and updates are transmitted by the `Statistics`_ events.

Agent Events
============
Sheet
-----
* Cti.MessageType.SHEET

::

      {
       "msgType": "Sheet",
       "ctiMessage": {
         "timenow": 1425055334,
         "compressed": true,
         "serial": "xml",
         "payload": {
           "profile": {
             "user": {
               "sheetInfo": [
                 {
                   "value": "http://www.google.fr/",
                   "name": "popupUrl",
                   "order": 10,
                   "type": "url"
                 },
                 {
                   "value": "&folder=1234",
                   "name": "folderNumber",
                   "order": 30,
                   "type": "text"
                 },
                 {
                   "value": "http://www.google.fr/",
                   "name": "popupUrl1",
                   "order": 20,
                   "type": "url"
                 }
               ]
             }
           }
         },
         "channel": "SIP/1k4yj2-00000013"
       }
      }

Right Profile
-------------
* Cti.MessageType.RIGHTPROFILE: "RightProfile"

::

   {"msgType":"RightProfile","ctiMessage":{"profile":"Supervisor", "rights":["dissuasion","recording"]}}


This message is sent upon connection to the xuc websocket. The profile can be one of: "Supervisor", "Admin", "NoRight".
The rights property contains an array with additional rights:

* "recording": User has access to recording management settings on queues
* "dissuasion": User has access to dissuasion management settings on queues


State Event
-----------
* Cti.MessageType.AGENTSTATEEVENT

    * AgentLogin (DEPRECATED: Agent are now going directly from AgentLoggedOut to AgentReady)

    ::

    {"name":"AgentLogin","agentId":19,"phoneNb":"1000","since":1423839787,"queues":[8,14,170,4,1]}

    * AgentReady

    ::

    {"name":"AgentReady","agentId":19,"phoneNb":"1000","since":0,"queues":[8,14,170,4,1],"cause":"available"}

    * AgentOnPause

    ::

    {"name":"AgentOnPause","agentId":19,"phoneNb":"1000","since":0,"queues":[8,14,170,4,1],"cause":"available"}

    * AgentOnWrapup

    ::

    {"name":"AgentOnWrapup","agentId":19,"phoneNb":"1000","since":2,"queues":[8,14,170,4,1]}

    * AgentRinging

    ::

    {"name":"AgentRinging","agentId":19,"phoneNb":"1000","since":0,"queues":[8,14,170,4,1]}

    * AgentDialing

    ::

    {"name":"AgentDialing","agentId":19,"phoneNb":"1000","since":0,"queues":[8,14,170,4,1]}

    * AgentOnCall

    ::

        {"msgType":"AgentStateEvent","ctiMessage":
            {"name":"AgentOnCall","agentId":19,"phoneNb":"1000","since":0,"queues":[8,14,170,4,1],
                "acd":false,"direction":"Incoming","callType":"External","monitorState":"ACTIVE"}}



    * AgentLoggedOut

    ::

    {"name":"AgentLoggedOut","agentId":19,"phoneNb":"1000","since":0,"queues":[8,14,170,4,1]}


Agent Error
-----------
* Cti.MessageType.AGENTERROR

Agent Directory
---------------
* Cti.MessageType.AGENTDIRECTORY

Triggered by command `Cti.getAgentDirectory`

::

    {"directory": [
        { "agent":
            {"context": "default", "firstName": "bj", "groupId": 1, "id": 8, "lastName": "agent", "number": "2000"},
            "agentState": {"agentId": 8, "cause": "", "name": "AgentReady", "phoneNb": "1001", "queues": [1, 2], "since": 2 }}]}


Agent Configuration
-------------------
* AGENTCONFIG: "AgentConfig"

Triggered when agent configuration changes

::

    {"id":23,"firstname":"Jack","lastname":"Flash","number":"2501","context":"default","member": [
        {"queue_name":"queue1","queue_id":1,"interface":"Agent/2501","penalty":1,"commented":0,"usertype":"Agent","userid":1,"channel":"Agent","category":"Queue","position":1},
        {"queue_name":"queue2","queue_id":2,"interface":"Agent/2501","penalty":2,"commented":0,"usertype":"Agent","userid":1,"channel":"Agent","category":"Queue","position":1}],
    "numgroup":1,"userid":1}

Agent List
----------
* Cti.MessageType.AGENTLIST

Receives agent configuration list in a javascript Array : Command `Cti.getList("agent");`

::

    [
        {"id":24,"firstName":"John","lastName":"Waynes","number":"2601","context":"default","groupId":1},
        {"id":20,"firstName":"Maricé","lastName":"Saprïtchà","number":"2602","context":"default","groupId":1},
        {"id":147,"firstName":"Etienne","lastName":"Burgad","number":"30000","context":"default","groupId":1},
        {"id":148,"firstName":"Caroline","lastName":"HERONDE","number":"29000","context":"default","groupId":2},
        {"id":149,"firstName":"Eude","lastName":"GARTEL","number":"75000","context":"default","groupId":3},
        {"id":22,"firstName":"Alice","lastName":"Johnson","number":"2058","context":"default","groupId":5}
    ]

Listen call
-----------

* AGENTLISTEN: "AgentListen",

Receives agent listen stop / start event, received automatically if user is an agent, no needs to subscribe.

::

    {"started":false,"phoneNumber":"1058","agentId":22}


Group List
----------
* AGENTGROUPLIST : "AgentGroupList"

Agent group list triggered by command : `Cti.getList("agentgroup")`

::

    [
        {"id":1,"name":"default"},
        {"id":2,"name":"boats"},
        {"id":3,"name":"broum"},
        {"id":4,"name":"bingba3nguh"},
        {"id":5,"name":"salesexpert"},
        {"id":6,"name":"a_very_long_group_name"}
    ]

Statistics
----------

Received on subscribe to agent statistics with method `Cti.subscribeToAgentStats()`_, current statistics are received automatically on subscribe.

* AGENTSTATISTICS : "AgentStatistics"

::

    {"id":22,
        "statistics":[
            {"name":"AgentPausedTotalTime","value":0},
            {"name":"AgentWrapupTotalTime","value":0},
            {"name":"AgentReadyTotalTime","value":434},
            {"name":"LoginDateTime","value":"2015-04-27T08:15:01.081+02:00"},
            {"name":"LogoutDateTime","value":"2015-04-27T08:14:49.427+02:00"}
            ]
    }


Callback Methods
================

Callback.getCallbackLists()
---------------------------

Retrieve the lists of callbacks with teir associated callback requests, and subscribe to callback events.

Callback.takeCallback(uuid)
---------------------------

Take the callback with the given uuid with the logged-in agent.

Callback.releaseCallback(uuid)
------------------------------

Release the callback which was previously taken

Callback.startCallback(uuid, phoneNumber)
-----------------------------------------

Launch the previously taken callback with the provided phone number.

Callback.updateCallbackTicket(uuid, status, description, dueDate, periodUuid)
-----------------------------------------------------------------------------

Update a callback ticket wih the provided description and status. Allowaed values for status are:

- NoAnswer
- Answered
- Fax
- Callback

dueDate is an optional parameter specifying the new due date using ISO format ("YYYY-MM-DD").

periodUuid is an optional parameter specifying the new preferred period for the callback.

Callback.listenCallbackMessage(uuid)
------------------------------------

Make your device rings to be able to listen recorded message if exists.

Callback Events
===============

Callback lists
--------------

Received when calling `Callback.getCallbackLists()`_.

* CALLBACKLISTS : "CallbackLists"

::

    {"uuid":"b0849ac0-4f4a-4ed0-9386-53ab2afd94b1",
     "name":"Liste de test",
     "queueId":1,
     "callbacks":[
       {"uuid":"a967da84-bc41-4bf4-a4fc-2bcc54e11606",
       "listUuid":"b0849ac0-4f4a-4ed0-9386-53ab2afd94b1",
       "phoneNumber":"0230210082",
       "mobilePhoneNumber":"0789654123",
       "firstName":"Alice",
       "lastName":"O'Neill",
       "company":"YourSociety",
       "description":null,
       "agentId":null,
       "dueDate": "2016-08-01",
       "preferredPeriod": {
          "default": false,
          "name": "Afternoon",
          "periodStart": "14:00:00",
          "periodEnd": "17:00:00",
          "uuid": "d3270038-e20e-498a-af71-3cf69b5cc792"
       }}
     ]}

Callback Taken
--------------

Received after taking a callback with `Callback.takeCallback(uuid)`_.

* CALLBACKTAKEN : "CallbackTaken"

::

    {"uuid":"a967da84-bc41-4bf4-a4fc-2bcc54e11606",
     "agentId":2}

Callback Started
----------------

Received after starting a callback with `Callback.startCallback(uuid, phoneNumber)`_.

* CALLBACKSTARTED : "CallbackStarted"

::

    {"requestUuid":"a967da84-bc41-4bf4-a4fc-2bcc54e11606",
     "ticketUuid":"8e82de0f-847a-4606-97bf-bef5a18ea8b0"}

Callback Clotured
-----------------

Received after giving to a callback a status different of ``Callback``.

* CALLBACKCLOTURED : "CallbackClotured"

::

    {"uuid":"a967da84-bc41-4bf4-a4fc-2bcc54e11606"}

Callback Released
-----------------

Received after releasing a callback with `Callback.releaseCallback(uuid)`_.

* CALLBACKRELEASED : "CallbackReleased"

::

    {"uuid":"a967da84-bc41-4bf4-a4fc-2bcc54e11606"}

Callback Updated
----------------

Received when calling `Callback.updateCallbackTicket(uuid, status, description, dueDate, periodUuid)`_ with a new due date or period.

* CALLBACKREQUESTUPDATED : "CallbackRequestUpdated"

::

    {"request":{
       "uuid":"a967da84-bc41-4bf4-a4fc-2bcc54e11606",
       "listUuid":"b0849ac0-4f4a-4ed0-9386-53ab2afd94b1",
       "phoneNumber":"0230210082",
       "mobilePhoneNumber":"0789654123",
       "firstName":"Alice",
       "lastName":"O'Neill",
       "company":"YourSociety",
       "description":null,
       "agentId":null,
       "dueDate": "2016-08-01",
       "preferredPeriod": {
          "default": false,
          "name": "Afternoon",
          "periodStart": "14:00:00",
          "periodEnd": "17:00:00",
          "uuid": "d3270038-e20e-498a-af71-3cf69b5cc792"
       }
    }}
