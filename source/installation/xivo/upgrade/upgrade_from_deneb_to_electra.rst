************************
Upgrade Deneb to Electra
************************

In this section is listed the manual steps to do when migrating from Deneb to Electra.

.. warning:: **XiVO UC/CC**: before upgrading to Electra you **MUST** upgrade the host to
             Debian **9 (Stretch)**. See :ref:`xivocc_upgrade_proc_deb_9`.


Before Upgrade
==============

* Custom container configurations

  * Compose file version was upgraded to version 3.7.
    See the `Compose file version 3 reference <https://docs.docker.com/compose/compose-file/>`_.
    If you have custom configurations, you should update them **before starting** the upgrade.
    Some of the previously used options, like `volumes_from`, can't be used anymore.
    To share data between containers, suggested procedure is:

    * use named volume (see the `Volume configuration reference <https://docs.docker.com/compose/compose-file/#volume-configuration-reference>`_)
    * to init the volume with data from the container, remove the container before starting it with the new configuration
    * to customize data in the volume, copy them with ``docker cp`` to the target container


On XiVO PBX
-----------

* XDS:

  * All media servers must be stopped with ``xivo-service stop all`` before upgrading XiVO.
    The rabbitmq exchange "xivo" was changed from "topic" to "x-delayed-message" type.
    If any media server is running, the exchange type may not be changed and XiVO will fail to start.
    If this happens, the rabbitmq container on XiVO must be stopped, removed and created again.


On XiVO CC
----------

.. warning:: before upgrading to Electra

     * you **MUST** upgrade the host to Debian **9 (Stretch)**.
       See :ref:`xivocc_upgrade_proc_deb_9`.
     * you **MUST** be using **Kibana in version 7** (the version that was released with Deneb LTS).
       If you did not yet upgrade Kibana to version 7 and remained using v3 you **MUST** upgrade it **before** upgrading to Electra.


After Upgrade
=============

On XiVO PBX
-----------

* **UC Addon** and **Chat History**: to have the Chat and Chat History feature working **you MUST** install the ``xivo-chat-backend`` package manually at the end of the upgrade.

  .. important:: To do so, follow instructions in :ref:`install_chat_backend` section.

* Switchboard: to be able to use the new Switchboard application **you MUST** replace your old switchboard configuration with the :ref:`new configuration <switchboard>`.


On XiVOCC
---------

* **Chat History**: to have the Chat and Chat History feature working **you MUST** install the ``xivo-chat-backend`` package manually at the end of the upgrade.

  .. important:: To do so, follow instructions in :ref:`install_chat_backend` section.

* Reporting: the last 7 days of data will be re-replicated to Elasticsearch, see :ref:`totem_data_flow`.
  It may take some time if you have a huge amout of calls per week (more than 1 hour if you have 2 million of queue_log per week).


On MDS
------

* NTP: you **MUST make sure** that your MDS use the XiVO Main as their NTP server (see :ref:`xds_mds-configuration`).
