***********************
Upgrade Five to Polaris
***********************

In this section are listed the manual steps to do when migrating from Five to Polaris.


Before Upgrade
==============

On XiVO PBX
-----------

Follow the :ref:`upgrade` page.


On XiVO CC
----------

The *xivo-solutions-VERSION* no longer exists. The ``xivocc-installer`` package is now located in *xivo-VERSION* distribution.
You have to update your source list accordingly.


#. Remove apt source list file::

    rm /etc/apt/sources.list.d/xivo-solutions.list

#. Add new source list file::

    echo "deb http://mirror.xivo.solutions/debian xivo-polaris main" > /etc/apt/sources.list.d/xivo-dist.list


After Upgrade
=============

On XiVO PBX
-----------

* Accept new cel.conf: if you are asked by ``xivo-upgrade`` installer, you must choose to replace the ``cel.conf`` file or ensure
  that its content correspond to these :ref:`defaults <cel>`.

* Finish to remove xivo-ctid-ng and xivo-websocketd::

   apt-get purge xivo-websocketd xivo-ctid-ng

* Add `writetimeout` parameter to the the :file:`/etc/asterisk/manager.d/02-xivocc.conf` file:

      .. code-block:: bash
        :emphasize-lines: 7

        [xuc]
        secret = ...
        deny = ...
        permit = ...
        read = ...
        write = ...
        writetimeout = 10000

* You **MUST** update:

  * *Snom* phones to use plugin version **>=2.2** to be able to use CTI Transfer (UC Assistant or CCAgent),
  * *Yealink* phones to use plugin with **v81** firmware to be able to use CTI Transfer (UC Assistant or CCAgent).

* The WebRTC call limit was raised to 2 (to enable transfers). The simultcalls parameter of a WebRTC user should be set to 2 also.

On XiVO CC
----------

* Update new fingerboard::

   xivocc-dcomp stop fingerboard
   docker rm xivocc_fingerboard_1
   xivocc-dcomp up -d

* SpagoBI:
  
  * **Import new reports** as described in :ref:`spagobi` post install step
  * Then, you should also remove old sample reports:

    * Go to `Reports` menu and delete all reports which are located under **Racine -> Sample**
    * Once all reports are deleted inside these folder you can go to `Functionalities Management` menu as shown in following
      screenshot:

        .. figure:: reportsdelete1.png
             :scale: 100%

    * From here, you can delete empty folders by clicking on it

      .. figure:: reportsdelete2.png
           :scale: 100%

    * At the end you should have a report folder list, that contains only `Rapports` and `Accueil` folder as seen here (unless
      you have some specific customer report):

      .. figure:: reportslist.png
           :scale: 100%

