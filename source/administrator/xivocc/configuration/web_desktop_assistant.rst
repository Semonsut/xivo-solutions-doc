.. _webassistant_configuration:

*************************
Web / Desktop Application
*************************

.. contents::


Common features
===============

.. _webassistant_disable_webrtc:

Disabling WebRTC
----------------

WebRTC can be disabled globally by setting the ``DISABLE_WEBRTC`` environment varibale to ``true`` in :file:`/etc/docker/compose/custom.env` file.


.. _webassistant_history_nbdays:

Call History Number of Days
---------------------------

Call history by default shows the last 7 days. You can change it by setting the ``CALL_HISTORY_NB_OF_DAYS`` environment variable to a specific number of days in the :file:`/etc/docker/compose/custom.env` file.

.. warning:: Note that setting this to a large number of days may slow down the solution.


.. _webassistant_disable_chat:

Disabling chat in UC Assistant and Switchboard
------------------------------------------------

The chat feature can be disabled globally by setting the ``DISABLE_CHAT`` environment varibale to ``true`` in :file:`/etc/docker/compose/custom.env` file.


.. _external_directory:

External Directory
------------------

To enable External Directory feature you need to configure it in the :file:`/etc/docker/compose/custom.env` file::

    EXTERNAL_VIEW_URL=https://myxivocc/externaldir


.. note:: When ``EXTERNAL_VIEW_URL`` is set, it will be displayed in both UC Assistant and CC Agent

.. warning::

  Take care of the following restrictions:

  * The ``EXTERNAL_VIEW_URL`` must be seen as hosted by XiVO CC platform (otherwise it won't open because of CORS restriction).
  * You MUST use the same HTTP protocol to access the CC application (UC Assistant or CC Agent) AND to access the external view.
    For example, if you access the application over HTTPS, you must access the external view over HTTPS too (to the avoid *Mixed Content* errors).
  * The external URL *MUST NOT* have the 'X-Frame-Options' to 'sameorigin', else the feature will not work (e.g. you can't use google.com as directory...).

.. _screen_popup:

Screen Popup
------------

It is possible to display customer information in an external web application using Xivo :ref:`sheet <custom-call-form>` mecanism.

* Go to :menuselection:`Services > CTI Server > Sheets > Models` to configure a sheet:

  * Tab *General Settings*: Give a name
  * Tab *Sheet*: You must define a sheet with at least ``folderNumber`` and ``popupUrl`` fields set:

    * ``folderNumber`` (MANDATORY)

      * field type = ``text``
      * It has to be defined. Can be calculated or use a default value not equal to "-"
      * Note: You could leave "empty" using a / symbol or using a whitespace (in hexadecimal: %20)

    * ``popupUrl`` (MANDATORY)

      * field type = ``text``
      * The url to open when call arrives : i.e. http://mycrm.com/customerInfo?folder= the folder number will be automatically
        appended at the end of the URL
      * Additionally to the existing xivo variables, you can also use here the following variables(only available in Web Agent and Desktop Agent):

        * ``{xuc-token}``: will be replaced by a token used for xuc websocket and rest api, for example ``http://mycrm.com/customerInfo?token={xuc-token}&folder=``
        * ``{xuc-username}``: will be replaced by the username of the logged on user, for example ``http://mycrm.com/customerInfo?username={xuc-username}&folder=``

    * ``multiTab`` (OPTIONAL)

      * field type = ``text``
      * set to the text ``true`` to open each popup in a new window.


* Then go to :menuselection:`Services > CTI Server > Sheets > Events` and choose the right events for opening the URL (if you choose two events, url will opened twice etc.)

Example : Using the caller number to open a customer info web page

* Define ``folderNumber`` with any default value i.e. 123456
* Define ``popupUrl`` with a display value of http://mycrm.com/customerInfo?nb={xivo-calleridnum}&fn= when call arrives web page http://mycrm.com/customerInfo?nb=1050&fn=123456 will be displayed

    .. figure:: example_xivo_sheet.png
      :scale: 90%

.. _screen_popup_ucassistant:

Screen popup on UC Assistant
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

By default the sheet:

* on *CC Agent* application the sheet is opened by default,
* on *UC Assistant* application the sheet is **not** opened by default.

You can change the behavior with the following sheet variables:

* ``popupUCActivated``:

  * if set to ``true`` the sheet will be opened on *UC Assistant* application
  * if set to ``false`` (default) the sheet won't be opened on *UC Assistant* application

* ``popupAgentActivated``:

  * if set to ``true`` (default) the sheet will be opened on *CC Agent* application
  * if set to ``false`` the sheet won't be opened on *CC Agent* application


For example, if you want the sheet to **only** open on UC Assistant application you should add in your sheet configuration:

* in :menuselection:`Services > CTI Server > Sheets > Models`:

  * Tab *Sheet* add the following definition:

    * ``popupAgentActivated``

      * field type = ``text``
      * display value = ``false``
    * ``popupUCActivated``

      * field type = ``text``
      * display value = ``true``


.. note:: These variables can also be filled via a dialplan variable value with the ``UserEvent`` application and the ``{dp-...}`` syntax mechanism.
    See the XiVO PBX :ref:`sheet description <custom-call-form>`.


Desktop Assistant Specific Features
===================================

.. _run_executable_sheet:

Run executable
--------------

It is also possible to run an executable using Xivo :ref:`sheet <custom-call-form>` mecanism. This is only available in the desktop agent and desktop assistant.

.. warning:: For the executable to be run on a Desktop Assistant in **UC mode**, you need to activate the :ref:`screen_popup_ucassistant`
    (in **CC Agent mode** the screen popup doesn't need to be activated and therefore the executable will be run out-of-the-box).

* Go to :menuselection:`Services > CTI Server > Sheets > Models`:

  * Tab *General Settings*: Give a name
  * Tab *Sheet*: You must define a sheet with at least ``runAsExecutable`` and ``popupUrl`` fields set:

    * ``popupUrl`` (MANDATORY)

      * field type = ``text``
      * It should contain an executable name accessible by the client user (where the desktop application is) or a full executable path.

    * ``runAsExecutable`` (MANDATORY)

      * field type = ``text``
      * Display value ``true``

    * ``executableArgs`` (OPTIONAL)

      * field type = ``text``
      * set the argument for the executable.


* Then go to :menuselection:`Services > CTI Server > Sheets > Events` and choose the right events for starting the application.

Example : Run the ``notify-send`` command on linux:

* Define ``popupUrl`` with a display value of ``notify-send``
* Define ``runAsExecutable`` with a display value of ``true``
* Define ``executableArgs`` with a display value of ``caller:{xivo-calleridnum}`` where the variable ``xivo-calleridnum`` will be replaced by the caller phone number.

.. figure:: example_xivo_sheet_exe.png
    :scale: 90%
