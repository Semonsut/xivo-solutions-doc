**********************************
XiVO Electra Intermediate Versions
**********************************

2020.06
=======

Consult the `2020.06 Roadmap <https://projects.xivo.solutions/versions/188>`_.

Components updated: **asterisk**, **xivo-agid**, **xivo-config**, **xivo-provd-plugins**, **xivo-upgrade**, **xivo-web-interface**, **xivocc-installer**, **xucmgt**, **xucserver**

**Asterisk**

* `#3150 <https://projects.xivo.solutions/issues/3150>`_ - Asterisk crash after video call is answered
* `#3217 <https://projects.xivo.solutions/issues/3217>`_ - Build Asterisk 16.9.0

**Chat**

* `#3160 <https://projects.xivo.solutions/issues/3160>`_ - As an admin I can install and configure chat backend on uc-addon
* `#3183 <https://projects.xivo.solutions/issues/3183>`_ - Receive notification for unread messages on login
* `#3184 <https://projects.xivo.solutions/issues/3184>`_ - Be able to open chat and write to every xivo user even offline
* `#3189 <https://projects.xivo.solutions/issues/3189>`_ - When I open chat with unread messages I want them be marked as read
* `#3190 <https://projects.xivo.solutions/issues/3190>`_ - When I login I want to see unread chats listed under chat window
* `#3191 <https://projects.xivo.solutions/issues/3191>`_ - When I read all my unread messages I should see no more unread notification badge
* `#3195 <https://projects.xivo.solutions/issues/3195>`_ - When I login I want to see unread chats listed under chat window [frontend part]
* `#3203 <https://projects.xivo.solutions/issues/3203>`_ - Chat - Empty message

**Desktop Assistant**

* `#3208 <https://projects.xivo.solutions/issues/3208>`_ - [C] - callto/tel links containing spaces are not handled properly in Chrome

**DevSpe**

* `#3212 <https://projects.xivo.solutions/issues/3212>`_ - Webpanels - update mount configuration to compose file version 3.7

**Switchboard**

* `#3204 <https://projects.xivo.solutions/issues/3204>`_ - Small display problem for long name in hold/incoming queue
* `#3205 <https://projects.xivo.solutions/issues/3205>`_ - Switchboard - display the 'retrieve' icon when using kbd shortcuts on calls on hold

**XUC Server**

* `#3215 <https://projects.xivo.solutions/issues/3215>`_ - Add xuc-cli to interact with running xuc server

**XiVO PBX**

* `#2619 <https://projects.xivo.solutions/issues/2619>`_ - Remove unused tabs and parameters from CTI Sheet configuration

  .. important:: **Behavior change** Some parts of form Models -> Sheets were removed. They are not currently used by any of our services, so there shouldn't be any impact.

* `#3026 <https://projects.xivo.solutions/issues/3026>`_ - Doc - Agent context must be the same as user's line context
* `#3158 <https://projects.xivo.solutions/issues/3158>`_ - As a user with UA activated my WebRTC line rings when I'm connected to my Web Assistant
* `#3163 <https://projects.xivo.solutions/issues/3163>`_ - As a user with UA I should be able to check my voicemail
* `#3177 <https://projects.xivo.solutions/issues/3177>`_ - As a user with UA I should have a notification from my voicemail
* `#3187 <https://projects.xivo.solutions/issues/3187>`_ - Update Cisco plugin to add information about specific firmware download procedure
* `#3194 <https://projects.xivo.solutions/issues/3194>`_ - Old tokens from xivo-auth may not be deleted
* `#3218 <https://projects.xivo.solutions/issues/3218>`_ - Webi - the context list for parameter Exit Context does not list the contexts of type Service

  .. important:: **Behavior change** Queue parameter Exit Context can now be set to context of type Service


2020.05
=======

Consult the `2020.05 Roadmap <https://projects.xivo.solutions/versions/187>`_.

Components updated: **asterisk**, **xivo-confd**, **xivo-confgend**, **xivo-monitoring**, **xivo-web-interface**, **xucmgt**, **xucserver**

**Asterisk**

* `#3156 <https://projects.xivo.solutions/issues/3156>`_ - Build asterisk 16.8.0

**Chat**

* `#2717 <https://projects.xivo.solutions/issues/2717>`_ - Limit disk space used by Mattermost
* `#3121 <https://projects.xivo.solutions/issues/3121>`_ - As a user when I open a chat conversation with a user, I see the history of our exchanges
* `#3170 <https://projects.xivo.solutions/issues/3170>`_ - refactor toast display and utility for chat
* `#3173 <https://projects.xivo.solutions/issues/3173>`_ - Disable chat functionality has no effect
* `#3178 <https://projects.xivo.solutions/issues/3178>`_ - Retrieve chat history with offline user

**Switchboard**

* `#3128 <https://projects.xivo.solutions/issues/3128>`_ - Switchboard - As a switchboard operator I can retrieve a call from my hold queue calls list via keyboard shortcut

**XUC Server**

* `#3174 <https://projects.xivo.solutions/issues/3174>`_ - Multiple chat messages received for impatient user
* `#3185 <https://projects.xivo.solutions/issues/3185>`_ - Ghost calls in Web assistant & Web Agent
* `#3186 <https://projects.xivo.solutions/issues/3186>`_ - Make xuc aware of UA user/line so I can be logged in webrtc using my cti user account

**XiVO PBX**

* `#3133 <https://projects.xivo.solutions/issues/3133>`_ - Doc - As an administrator I can enable UA to a user
* `#3134 <https://projects.xivo.solutions/issues/3134>`_ - As a user with UA activated I can call from my Web Assistant with my WebRTC line
* `#3144 <https://projects.xivo.solutions/issues/3144>`_ -  Do not show wrong user's context in the incoming DID menu
* `#3172 <https://projects.xivo.solutions/issues/3172>`_ - Import / Update via CSV massively reload asterisk configuration (instead of reloading it once)
* `#3175 <https://projects.xivo.solutions/issues/3175>`_ - [C] - Enhance CSV update/import by adding a message in confd logs when import/update is actually finished
* `#3181 <https://projects.xivo.solutions/issues/3181>`_ - Generate WebRTC line for UA user
* `#3188 <https://projects.xivo.solutions/issues/3188>`_ - XDS - Monit should supervise asterisk on MDS


2020.04
=======

Consult the `2020.04 Roadmap <https://projects.xivo.solutions/versions/183>`_.

Components updated: **mattermost-docker-xivo-mirror**, **pack-reporting**, **packaging**, **rabbitmq**, **xivo-confd**, **xivo-switchboard-reports**, **xivocc-installer**, **xucmgt**, **xucserver**

**CCAgent**

* `#3139 <https://projects.xivo.solutions/issues/3139>`_ - Integrating UI grid library in CCagent, switchboard and UC assistant

**Chat**

* `#2861 <https://projects.xivo.solutions/issues/2861>`_ - Automate MattermostDb creation
* `#2873 <https://projects.xivo.solutions/issues/2873>`_ - Persist FlashText into private message channels in Mattermost
* `#3122 <https://projects.xivo.solutions/issues/3122>`_ - Update mattermost to the latest stable version
* `#3152 <https://projects.xivo.solutions/issues/3152>`_ - As an admin I want to be able to disable the chat feature

**Desktop Assistant**

* `#3147 <https://projects.xivo.solutions/issues/3147>`_ - error messages in desktop app contact page cause display bug

**Reporting**

* `#3094 <https://projects.xivo.solutions/issues/3094>`_ - Add `xc_` tables to existing purge mechanism

**Switchboard**

* `#3146 <https://projects.xivo.solutions/issues/3146>`_ - Switchboard display improvements
* `#3148 <https://projects.xivo.solutions/issues/3148>`_ - Keyboard shortcuts are not unregistered when loggging out CC Agent / Switchboard / UC Assistant
* `#3149 <https://projects.xivo.solutions/issues/3149>`_ - Switchboard - Reports service does not recover after db restart
* `#3151 <https://projects.xivo.solutions/issues/3151>`_ - Switchboard - As a switchboard operator I should be able to start a chat conversation with users of the company
* `#3154 <https://projects.xivo.solutions/issues/3154>`_ - Integrate the new chat module into the switchboard and adapt the display

**Web Assistant**

* `#3153 <https://projects.xivo.solutions/issues/3153>`_ - Refactor the current chat structure into an xcchat module
* `#3155 <https://projects.xivo.solutions/issues/3155>`_ - Re-integrate the chat module in ucassistant if needed and make sure it works

**XUC Server**

* `#3169 <https://projects.xivo.solutions/issues/3169>`_ - Some users can't receive FlashText

**XiVO PBX**

* `#3114 <https://projects.xivo.solutions/issues/3114>`_ - XDS - Duplicate reload commands on main
* `#3166 <https://projects.xivo.solutions/issues/3166>`_ - Enhance CSV update/import by removing the duplicate calls to provd

2020.03
=======

Consult the `2020.03 Roadmap <https://projects.xivo.solutions/versions/182>`_.

Components updated: **xivo-agid**, **xivo-auth**, **xivo-confgend**, **xivo-config**, **xivo-ctid**, **xivo-monitoring**, **xivo-service**, **xivo-switchboard-reports**, **xivo-upgrade**, **xivo-web-interface**, **xivocc-installer**, **xucmgt**, **xucserver**

**CCManager**

* `#3118 <https://projects.xivo.solutions/issues/3118>`_ - [C] - realtime statistics thresholds when changed in conf are not displayed in CC Manager

**Desktop Assistant**

* `#3115 <https://projects.xivo.solutions/issues/3115>`_ - Desktop assistant doesn't start at the end of installation on Windows
* `#3119 <https://projects.xivo.solutions/issues/3119>`_ - Squirrel flood logs with errors on first start after install
* `#3127 <https://projects.xivo.solutions/issues/3127>`_ - Cannot access to webview logs in desktop assistant

**DevSpe**

* `#3105 <https://projects.xivo.solutions/issues/3105>`_ - [OPT] Refresh gateway recording to be used with new CC recording

**Mobile Application**

* `#3083 <https://projects.xivo.solutions/issues/3083>`_ - API : When I call a conference with my mobile I enter the conference as the administrator

  .. important:: **Behavior change** When using the Mobile Application to enter in a conference, you are automatically identified as administrator of the conference if an admin pin has been defined in the conference.

* `#3091 <https://projects.xivo.solutions/issues/3091>`_ - API : When I call a conference with my mobile I can see that I'm in a conference

  .. important:: **Behavior change** When using the Cti.dialFromMobile api, you are now automatically subscribed to phone events and conference events of the defined mobile phone number.


**Switchboard**

* `#3045 <https://projects.xivo.solutions/issues/3045>`_ - Switchboard - Describe the different scenario for call handling with keyboard
* `#3089 <https://projects.xivo.solutions/issues/3089>`_ - As a XiVO Admin I can download a CSV export of the switchboards queues statistics via the XiVO Webi
* `#3106 <https://projects.xivo.solutions/issues/3106>`_ - As a switchboard operator I can transfer a call from the search results with the keyboard
* `#3109 <https://projects.xivo.solutions/issues/3109>`_ - Switchboard - Validate the Switchboard with our supported phones

  .. important:: **Behavior change** Removed switchboard tickbox in web interface devices menu, we do not need this option anymore.

* `#3112 <https://projects.xivo.solutions/issues/3112>`_ - Switchboard - Change the jasper report jrxml file to get all switchboard queues
* `#3113 <https://projects.xivo.solutions/issues/3113>`_ - Switchboard - Webi to execute jasper with reports file

  .. important:: **Behavior change** Switchboard statistics report changed format to PDF.

* `#3130 <https://projects.xivo.solutions/issues/3130>`_ - The search bar should empty itself when i press enter to call a number in the search bar

  .. important:: **Behavior change** When pressing enter to call a number from the search bar, the number is removed from the search bar


**Web Assistant**

* `#2148 <https://projects.xivo.solutions/issues/2148>`_ - Prevent personal contact creation without name or surname
* `#3125 <https://projects.xivo.solutions/issues/3125>`_ - UCassistant try to retrrieve showQueueControl and fails at login
* `#3129 <https://projects.xivo.solutions/issues/3129>`_ - [C] - History display on ucassistant on Windows is broken (the date overlap the clock)

**WebRTC**

* `#2379 <https://projects.xivo.solutions/issues/2379>`_ - Support WebRTC SDP Unified plan
* `#3132 <https://projects.xivo.solutions/issues/3132>`_ - Audio stream is not released when rejecting a call

**XUC Server**

* `#3135 <https://projects.xivo.solutions/issues/3135>`_ - Qualification download from the ccmanager timeout

**XiVO PBX**

* `#2075 <https://projects.xivo.solutions/issues/2075>`_ - Hide the option "Use n+101 method" from the menu user -> no answer
* `#2503 <https://projects.xivo.solutions/issues/2503>`_ - Finish IAX removal
* `#3095 <https://projects.xivo.solutions/issues/3095>`_ - Doc - Caller Number Normalization
* `#3123 <https://projects.xivo.solutions/issues/3123>`_ - xivo-auth and rabbitmq (erlang) take a lot of RAM and cause XiVO overload

**XiVOCC Infra**

* `#3093 <https://projects.xivo.solutions/issues/3093>`_ - Speed up docker images PROD build by tagging after build succeeds


2020.02
=======

Consult the `2020.02 Roadmap <https://projects.xivo.solutions/versions/181>`_.

Components updated: **xivo-agentd**, **xivo-agid**, **xivo-amid**, **xivo-auth**, **xivo-call-logs**, **xivo-confd**, **xivo-ctid**, **xivo-db**, **xivo-dird**, **xivo-lib-python**, **xivo-sysconfd**, **xivo-tools**, **xivo-upgrade**, **xivo-web-interface**, **xucmgt**, **xucserver**

**Desktop Assistant**

* `#3102 <https://projects.xivo.solutions/issues/3102>`_ - Migration from older version to newest version cause electron localstorage problems

**DevSpe**

* `#3108 <https://projects.xivo.solutions/issues/3108>`_ - [OPT] Be able to deactivate the stop/start recording depending the queue recording mode

  .. important:: **Behavior change** XUC recording rules action can be disabled by setting the variable `ENABLE_RECORDING_RULES` to false in docker-xivocc.yml.


**Switchboard**

* `#3031 <https://projects.xivo.solutions/issues/3031>`_ - Switchboard - After transferring to waiting queue update caller name and number
* `#3032 <https://projects.xivo.solutions/issues/3032>`_ - Switchboard - When Switchboard operator delog/relog, the incoming calls list and hold calls list should be displayed correctly
* `#3033 <https://projects.xivo.solutions/issues/3033>`_ - Switchboard - Should handle retrieve when another call is ringing or ongoing
* `#3081 <https://projects.xivo.solutions/issues/3081>`_ - Switchboard - Have basic switchboard statistics
* `#3087 <https://projects.xivo.solutions/issues/3087>`_ - Create switchboard statistics report in jrxml format
* `#3090 <https://projects.xivo.solutions/issues/3090>`_ - Switchboard - As a switchboard operator I can choose an user with keyboard arrows
* `#3092 <https://projects.xivo.solutions/issues/3092>`_ - Switchboard - No focus on search bar after answer
* `#3097 <https://projects.xivo.solutions/issues/3097>`_ - Switchboard : update documentation

**XiVO PBX**

* `#2325 <https://projects.xivo.solutions/issues/2325>`_ - After changing line of user, queue membership in asterisk is incorrect
* `#2937 <https://projects.xivo.solutions/issues/2937>`_ - Adding line and queue membership at once is possible only with SIP protocol (it is broken for custom and sccp lines)

  .. important:: **Behavior change** Adding a line and a group/queue membership to a user at once is possible only with SIP protocol. With SCCP or custom line, you must save the user before setting the group/queue membership.

* `#2953 <https://projects.xivo.solutions/issues/2953>`_ - XDS - Asterisk can keep old configuration on MDS after editation from webi

2020.01
========

Consult the `2020.01 Roadmap <https://projects.xivo.solutions/versions/179>`_.

Components updated: **pack-reporting**, **xivo-config**, **xivo-outcall**, **xivo-upgrade**, **xivocc-installer**, **xucmgt**, **xucserver**

**Reporting**

* `#3080 <https://projects.xivo.solutions/issues/3080>`_ - [C] - log rotation for specific-stats.log does not work as expected

**Switchboard**

* `#2949 <https://projects.xivo.solutions/issues/2949>`_ - Switchboard - As a switchboard agent I can send an ongoing call to my hold queue
* `#3021 <https://projects.xivo.solutions/issues/3021>`_ - Switchboard App - List of calls (in incoming/hold queue) is cleared if connection to one of the MDS is lost
* `#3030 <https://projects.xivo.solutions/issues/3030>`_ - Switchboard - Doc - Retrieved call is seen as outgoing and non-acd
* `#3040 <https://projects.xivo.solutions/issues/3040>`_ - Switchboard - As a switchboard operator I can have my own hold queue
* `#3044 <https://projects.xivo.solutions/issues/3044>`_ - Switchboard - As a switchboard operator I can do a direct transfer to a user/number
* `#3046 <https://projects.xivo.solutions/issues/3046>`_ - Switchboard - As a switchboard operator I get an error message if there is a mis-configuration
* `#3064 <https://projects.xivo.solutions/issues/3064>`_ - Switchboard - As a switchboard operator I can do a direct transfer to the number in search bar
* `#3068 <https://projects.xivo.solutions/issues/3068>`_ - Switchboard - As a switchboard operator I can do a direct transfer to an user in my search results
* `#3071 <https://projects.xivo.solutions/issues/3071>`_ - Switchboard - Add new shortcut for send to hold queue button
* `#3072 <https://projects.xivo.solutions/issues/3072>`_ - Switchboard - Add queue log event when retrieve from hold queue to fix statistics
* `#3084 <https://projects.xivo.solutions/issues/3084>`_ - Switchboard scenario : focus on answer, hangup and send to the hold queue

**Web Assistant**

* `#3062 <https://projects.xivo.solutions/issues/3062>`_ - Integrate external directory service inside ucassistant search bar
* `#3074 <https://projects.xivo.solutions/issues/3074>`_ - Add external view sample page in xucmgt

**XUC Server**

* `#2993 <https://projects.xivo.solutions/issues/2993>`_ - Remove SHOTGUN as only one dependency in xc_webrtc.js

**XiVO PBX**

* `#3057 <https://projects.xivo.solutions/issues/3057>`_ - Outcall can't be built due to year taken from now instead of mocked clock
* `#3060 <https://projects.xivo.solutions/issues/3060>`_ - Outcall - Remove deprecation warnings and update Scala version
* `#3063 <https://projects.xivo.solutions/issues/3063>`_ - Improve RabbitMQ install procedure

2019.15
=======

Consult the `2019.15 Roadmap <https://projects.xivo.solutions/versions/178>`_.

Components updated: **rabbitmq**, **xivo**, **xivo-amid**, **xivo-auth**, **xivo-config**, **xivo-ctid**, **xivo-monitoring**, **xivo-service**, **xivo-sysconfd**, **xivo-upgrade**, **xivo-web-interface**, **xivocc-installer**, **xucmgt**, **xucserver**

**Desktop Assistant**

* `#2971 <https://projects.xivo.solutions/issues/2971>`_ - Add spectron and tests for desktop application
* `#2978 <https://projects.xivo.solutions/issues/2978>`_ - split electron javascript file into a better multi-file architecture

  .. important:: **Behavior change** config and logs are now in %APPDATA%/xivo-desktop-assistant/application on windows and  $HOME/.config/xivo-desktop-assistant/application on linux

* `#3037 <https://projects.xivo.solutions/issues/3037>`_ - Global key is not working if clipboard is empty
* `#3043 <https://projects.xivo.solutions/issues/3043>`_ - [C] - Click to call crashes desktop application
* `#3053 <https://projects.xivo.solutions/issues/3053>`_ - add logs rotate to desktop app

**Switchboard**

* `#3036 <https://projects.xivo.solutions/issues/3036>`_ - Switchboard - I can configure the incoming call and holding call queue

**WebRTC**

* `#3041 <https://projects.xivo.solutions/issues/3041>`_ - Video calls works one way only (Caller doesn't see callee video)

**XiVO PBX**

* `#3000 <https://projects.xivo.solutions/issues/3000>`_ - XDS - Phone statuses are not correct after MDS restart
* `#3020 <https://projects.xivo.solutions/issues/3020>`_ - Dockerize and upgrade RabbitMQ on XiVO
* `#2631 <https://projects.xivo.solutions/issues/2631>`_ - [S] XSS in rabbitmq - upgrade to version >= 3.6.9

**XiVO Provisioning**

* `#2127 <https://projects.xivo.solutions/issues/2127>`_ - voicemail extension is not updated on provd , if we change value on web -i
* `#3029 <https://projects.xivo.solutions/issues/3029>`_ - Cannot create function key for T40G

2019.14
=======

Consult the `2019.14 Roadmap <https://projects.xivo.solutions/versions/169>`_.

Components updated: **asterisk**, **xivo-dird**, **xivo-dist**, **xivo-full-stats**, **xivocc-installer**, **xucmgt**, **xucserver**

**Asterisk**

* `#2924 <https://projects.xivo.solutions/issues/2924>`_ - Add AMI Refer command to handle transfers
* `#3005 <https://projects.xivo.solutions/issues/3005>`_ - Build asterisk 16.3.0 with patch #2924

**CCAgent**

* `#2951 <https://projects.xivo.solutions/issues/2951>`_ - As a ccAgent I can use keyboard shortcuts
* `#3035 <https://projects.xivo.solutions/issues/3035>`_ - Display the custom pause status in agent list

**Reporting**

* `#2243 <https://projects.xivo.solutions/issues/2243>`_ - Reporting server has wrong information about agent login status

**Switchboard**

* `#2950 <https://projects.xivo.solutions/issues/2950>`_ - As a Switchboard agent I can retrieve a call from the hold queue

**XUC Server**

* `#3010 <https://projects.xivo.solutions/issues/3010>`_ - Create API inside XUC to retrieve a call through its call id

**XiVO Provisioning**

* `#2969 <https://projects.xivo.solutions/issues/2969>`_ - [C] - Snom, directory lookup does not work for phones with fw 10 (and above)
* `#3012 <https://projects.xivo.solutions/issues/3012>`_ - [C] - Loud speaker is activated when initiated attended transfer on Snom
* `#3015 <https://projects.xivo.solutions/issues/3015>`_ - Create Cisco plugin for SPA112/122 with fw v1.4.1 SR 5
* `#3016 <https://projects.xivo.solutions/issues/3016>`_ - [C] - Yealink T27G: can't add function keys on expansion module


2019.13
=======

Consult the `2019.13 Roadmap <https://projects.xivo.solutions/versions/167>`_.

Components updated: **recording-server**, **xivo-bus**, **xivo-confd**, **xivo-full-stats**, **xivo-provd-plugins**, **xivo-web-interface**, **xucmgt**, **xucserver**

**CCManager**

* `#2974 <https://projects.xivo.solutions/issues/2974>`_ - (C] - In agent view, when editing an agent, can't order on column

**Desktop Assistant**

* `#2977 <https://projects.xivo.solutions/issues/2977>`_ - conference error when reconnecting to ucassistant
* `#2985 <https://projects.xivo.solutions/issues/2985>`_ - Code sign Windows electron application to remove warnings of a potential threat
* `#3003 <https://projects.xivo.solutions/issues/3003>`_ - Install link for Dekstop Assistant is broken (only in dev)

**Recording**

* `#2961 <https://projects.xivo.solutions/issues/2961>`_ - [C] - Recorded calls which do not enter a queue are not displayed in the recording interface

**Reporting**

* `#2528 <https://projects.xivo.solutions/issues/2528>`_ - [C] - Call history does not show calls from Group

**Switchboard**

* `#2970 <https://projects.xivo.solutions/issues/2970>`_ - When logged to Switchboard application I see Switchboard application and display call from incoming and hold queues

**Web Assistant**

* `#2992 <https://projects.xivo.solutions/issues/2992>`_ - CallerID name display error when call comes from a Group

**XUC Server**

* `#2982 <https://projects.xivo.solutions/issues/2982>`_ - Be able to list calls in a queue from XuC API

**XiVO PBX**

* `#2640 <https://projects.xivo.solutions/issues/2640>`_ - Sip peers should reload on MDS when we create user on a mediaserver with APIs
* `#2925 <https://projects.xivo.solutions/issues/2925>`_ - [C] - Error when adding a user to a group when creating this user
* `#2964 <https://projects.xivo.solutions/issues/2964>`_ - [C] - Can't create connect/disconnect agent key
* `#2984 <https://projects.xivo.solutions/issues/2984>`_ - Asterisk should reload on MDS when we create object on a mediaserver with APIs

**XiVO Provisioning**

* `#2981 <https://projects.xivo.solutions/issues/2981>`_ - Create Snom plugin for fw v10.1.46.16

**XiVOCC Infra**

* `#3001 <https://projects.xivo.solutions/issues/3001>`_ - Fix xucmgt / desktop assistant build
