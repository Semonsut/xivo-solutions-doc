.. _xivosolutions_release:

*************
Release Notes
*************

.. _electra_release:

Electra (2020.07)
=================

Below is a list of *New Features* and *Behavior Changes* compared to the previous LTS version, Deneb (2019.12).


New Features
------------

**Assistants**:

* *Common features*:

  * **Chat conversation history**:

    * Can retrieve its chat history with another user when opening a chat conversation - see :ref:`how-to install it <install_chat_backend>` and associated features about :ref:`uc-assistant_chat`
    * Ability to send messages to offline users
  * Can deactivate the Chat feature - see :ref:`how-to <webassistant_disable_chat>`
  * Prevent personal contact creation without name or surname: a name (name or surname) and a number (number or mobile or other number) must be filled to be able to create a personal contact

* *WebRTC*:

  * When muted in a conference: I also see it in the sound gauge of my assistant, and the mute icon flashes red when I try to speak
* *CC Agent*:

  * Can use :ref:`uc_agent_shortcuts`
  * Custom pause status is displayed when hovering an agent in the Agents menu

**Desktop Assistant**:

* Windows executable file is now signed to avoid potential threat. Windows may still complain while installing until application is trusted by Microsoft.

**CC Manager**:

* Thresholds added in :ref:`statistics` can now be displayed in Queues view - see :ref:`ccmanager_thresholds`

**Recording**:

* Automatic stop/start recording on queues can be toggled with new configuration key ``ENABLE_RECORDING_RULES``

**Switchboard**: **new switchboard application compatible with XDS installation** - see :ref:`switchboard` documentation page for configuration and usage

  * New application accessible at https://XiVOCC/popc
  * Operator can receive/answer/transfer calls within the application
  * Operator can put calls in a holding queue to treat them when convenient
  * Operator can chose the call he wants to retrieve from the hold queue
  * Can do basic call control tasks with :ref:`keyboard shortcuts <switchboard_kbd_shortcuts>`
  * Can Chat with the other users of the company and send messages even if they are offline

**XDS**:

* Call to :ref:`confd REST API <confd-api>` now reloads asterisk configuration on all media servers
* Number of peers configured on each *MDS* are now displayed in :menuselection:`Services --> IPBX` page
* Each MDS has only its SIP peers loaded in asterisk
* Monit monitors asterisk on the MDS

**XiVO PBX**

* Import/Update enhancement: operation is faster and if it times out it displays a more comprehensive message to tell you what's happening - see :ref:`user_import` documentation
* Queue parameter :guilabel:`Exit Context` can now be set to context of type Service
* Limit of Agent Groups (:menuselection:`Services --> Call Center --> Agents`) was raised from 63 to 120
* Finish IAX removal from XiVO

**System**

* Asterisk version updated to 16.9.0
* Docker Compose files were upgraded to version 3.7
* Docker version was updated to version 18.06.3
* RabbitMQ version was updated to version 3.8
* *UC Addon*: lowered the default RAM value for JVM process


Behavior Changes
----------------

**Desktop Assistant**

* Config file and logs are now in the following directories:

 * Windows: :file:`%APPDATA%/xivo-desktop-assistant/application`
 * Linux: :file:`$HOME/.config/xivo-desktop-assistant/application`

**XDS**

* Each MDS has only its SIP peers loaded in asterisk

**XiVO PBX**

* Adding a line and a group/queue membership to a user at once is possible only with SIP protocol. With SCCP or custom line, you must save the user before setting the group/queue membership.
* Some parts of form :menuselection:`Services --> CTI Server --> Models --> Sheets` were removed as they were not used in our current CTI implementation.
* Queue parameter :guilabel:`Exit Context` can now be set to context of type Service
* Rabbitmq management web interface is no longer exposed (only on localhost)

**XiVO Client**

* XiVO Client application is not supported anymore on Electra. It was replaced by UC Assistant, CC Agent and the new Switchboard application.


Upgrade
-------

.. warning:: **Don't forget** the specific steps to upgrade to another LTS version - see :ref:`upgrade_lts_manual_steps`

Follow the usual upgrade procedures:

* :ref:`XiVO PBX upgrade procedure <upgrade>`
* :ref:`XiVO CC upgrade procedure <upgrade_cc>`
* :ref:`XDS upgrade procedure <upgrade_xds>`

Electra Bugfixes Versions
=========================

Components version table
------------------------

Table listing the current version of the components.

+----------------------+----------------+
| Component            | current ver.   |
+======================+================+
| **XiVO**                              |
+----------------------+----------------+
| XiVO PBX             | 2020.07.00     |
+----------------------+----------------+
| config_mgt           | 2020.07.00     |
+----------------------+----------------+
| db                   | 2020.07.00     |
+----------------------+----------------+
| outcall              | 2020.07.00     |
+----------------------+----------------+
| db_replic            | 2020.07.00     |
+----------------------+----------------+
| nginx                | 2020.07.00     |
+----------------------+----------------+
| webi                 | 2020.07.00     |
+----------------------+----------------+
| **XiVO CC**                           |
+----------------------+----------------+
| elasticsearch        | 7.3.1          |
+----------------------+----------------+
| kibana               | 7.3.1          |
+----------------------+----------------+
| logstash             | 2020.07.00     |
+----------------------+----------------+
| nginx                | 2020.07.00     |
+----------------------+----------------+
| pack-reporting       | 2020.07.00     |
+----------------------+----------------+
| pgxivocc             | 1.3            |
+----------------------+----------------+
| recording-rsync      | 1.0            |
+----------------------+----------------+
| recording-server     | 2020.07.00     |
+----------------------+----------------+
| spagobi              | 2020.07.00     |
+----------------------+----------------+
| xivo-full-stats      | 2020.07.00     |
+----------------------+----------------+
| xuc                  | 2020.07.00     |
+----------------------+----------------+
| xucmgt               | 2020.07.00     |
+----------------------+----------------+

Electra.00
----------

.. note:: **LTS Release**. New features and behavior changes are listed above under the :ref:`electra_release` section.

Consult the `Electra (2020.07) Roadmap <https://projects.xivo.solutions/versions/160>`_.

Components updated: **config-mgt**, **rabbitmq**, **recording-server**, **xivo-confgend**, **xivo-dao**, **xivo-db**, **xivo-manage-db**, **xivo-purge-db**, **xivo-web-interface**, **xivocc-installer**, **xivoxc-nginx**, **xucmgt**, **xucserver**

**Asterisk**

* `#3214 <https://projects.xivo.solutions/issues/3214>`_ - No audio in WebRTC second call if user is behind a VPN

**Chat**

* `#3225 <https://projects.xivo.solutions/issues/3225>`_ - Chat - False error notification if chat history never existed

**Mobile Application**

* `#3082 <https://projects.xivo.solutions/issues/3082>`_ - As a mobile app user I Join the conference as administrator

**Reporting**

* `#3207 <https://projects.xivo.solutions/issues/3207>`_ - Doc - Logstash re-replicates last week of data after upgrade
* `#3223 <https://projects.xivo.solutions/issues/3223>`_ - Add webservice for CEL retrieval from CC database

**Security**

* `#2622 <https://projects.xivo.solutions/issues/2622>`_ - [S] Rabbitmq exposes a non secured http based admin interface

**Switchboard**

* `#3221 <https://projects.xivo.solutions/issues/3221>`_ - Callerid and Waiting time are not aligned in incoming calls list
* `#3238 <https://projects.xivo.solutions/issues/3238>`_ - Change URL to connect to the switchboard from /switchboard to /popc
* `#3240 <https://projects.xivo.solutions/issues/3240>`_ - Logged Switchboard users get redirected to CCAgent when a new version is released

**Web Assistant**

* `#3216 <https://projects.xivo.solutions/issues/3216>`_ - Footer moves when conference participants list does not fit the window and the user scrolls
* `#3220 <https://projects.xivo.solutions/issues/3220>`_ - I should see that I'm muted in the xivo assistant
* `#3230 <https://projects.xivo.solutions/issues/3230>`_ - the name of the person in a call with me is not fully displayed unless i hover it
* `#3246 <https://projects.xivo.solutions/issues/3246>`_ - Chat - Sort conversation per date of received message
* `#3247 <https://projects.xivo.solutions/issues/3247>`_ - Chat - Display the day of the message (not only the hour)

**XUC Server**

* `#3224 <https://projects.xivo.solutions/issues/3224>`_ - XDS - Voicemail notification badge is random
* `#3232 <https://projects.xivo.solutions/issues/3232>`_ - Agent may not have dissuasion rights after login if he also uses UC Assistant

**XiVO PBX**

* `#2438 <https://projects.xivo.solutions/issues/2438>`_ - XDS - As an Admin, I'd like to easily know the Media Server that exist and the number of users they host
* `#2524 <https://projects.xivo.solutions/issues/2524>`_ - XDS - Each MDS should only have its own SIP peers
* `#3161 <https://projects.xivo.solutions/issues/3161>`_ - As a user with UA activated I should see history of both my phone and webrtc line
* `#3162 <https://projects.xivo.solutions/issues/3162>`_ - As a user with UA I should have only one presence state displayed for other users
* `#3164 <https://projects.xivo.solutions/issues/3164>`_ - UA - forwarding rule
* `#3176 <https://projects.xivo.solutions/issues/3176>`_ - Enhance CSV update/import by clarifying timeout message
* `#3231 <https://projects.xivo.solutions/issues/3231>`_ - Raise limit of 63 agents groups to 120
* `#3236 <https://projects.xivo.solutions/issues/3236>`_ - stat_* tables should not be replicated on a MDS
* `#3241 <https://projects.xivo.solutions/issues/3241>`_ - CEL should be purged on MDS servers

**XiVOCC Infra**

* `#3250 <https://projects.xivo.solutions/issues/3250>`_ - Give JVM RAM configuration for UC Addon



Electra Intermediate Versions
=============================

.. toctree::
   :maxdepth: 2

   electra_iv
