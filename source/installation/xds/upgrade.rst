.. _upgrade_xds:

**************
Upgrading XDS
**************

Upgrading an XDS implies upgrading all components:

* XiVO (see :ref:`xds_xivo-upgrade`)
* Media Server (MDS) (one or more, see :ref:`xds_mds-upgrade`)


.. _xds_xivo-upgrade:

XiVO Server Upgrade
===================

#. Stop all services (``xivo-service stop all``) on **Media Servers** linked to your XiVO Main
#. Perform the upgrade as documented in :ref:`upgrade` section

.. _xds_mds-upgrade:

Media Server Upgrade
====================

.. important::
    * This procedure must be done on all media servers belonging to the upgraded XDS.
    * **Before upgrading a MDS, the MDS Main must be fully upgraded**


The upgrade process requires to run the following command on a shell prompt on each media server.

#. Switch version using ``xivo-dist`` utility and specifying the LTS or specific version you want to upgrade to. For example::

    xivo-dist xivo-deneb

#. Update package list::

    apt-get update

#. Stop all services::

    xivo-service stop all

#. Update configuration files::

    apt-get install xivo-config xivo-dist

#. Start updating the packages::

    apt-get dist-upgrade

#. Download new docker images::

    xivo-dcomp pull

#. Update and run containers::

    xivo-dcomp up -d --remove-orphans

#. Start the remaining services::

    xivo-service start

