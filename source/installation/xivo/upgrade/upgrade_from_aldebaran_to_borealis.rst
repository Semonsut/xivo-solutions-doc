*****************************
Upgrade Aldebaran to Borealis
*****************************

In this section is listed the manual steps to do when migrating from Aldebaran to Borealis.

.. warning:: For XiVO PBX Debian was upgraded to Debian 9 (stretch). Therefore:
    
     * the upgrade to Borealis will take longer than usual
     * upgrade from older version than XiVO 15.20 are not supported.

    Please read carefully :ref:`Debian 9 (stetch) upgrade notes <debian_9_upgrade_notes>` page.

.. warning:: **Known Upgrade Limitations:** as of Borealis release these are the known limitations for upgrade.
     These should be removed during the next bugfix releases of Borealis.

       * XiVO PBX / UC / CC is **not installable or upgradable on XFS partition created without** ``ftype=1`` **option**.
         If the partition is XFS, you **MUST** check if the option is enabled with the ``xfs_info`` command.
       * Upgrade for **XiVO CC** to Debian 9 **is currently not supported**.


Before Upgrade
==============

.. note:: If you have a *XiVO CC* or *XiVO UC* you **MUST** follow this upgrade order:

    #. Stop *XiVO CC / UC* services,
    #. Upgrade *XiVO PBX*
    #. Upgrade *XiVO CC / UC*: during the upgrade it will again restart the *XiVO PBX* services (to install the ``db_replic`` container)


On XiVO PBX
-----------

* Debian system will be upgraded to Debian 9 (stretch)

  .. warning:: Please **read carefully** the :ref:`Before the upgrade <debian_9_upgrade_notes_before>` section in the Debian 9 (stretch) upgrade notes.

* **UC Add-on**: during upgrade, *UC* database (`pgxivocc` container) will be removed.

  .. important:: Thus **all call history** (in *UC Assistant*) **will be lost**.

  The history will be computed again starting from the last call processed before the upgrade.
  If you want to recompute all the calls present in the *XiVO PBX* database you must follow a manual procedure which is not described here.


On XiVO CC
----------

* Before launching upgrade, you **MUST** verify that replication (of tables ``cel``, ``queue_log``, ``callback_request`` and ``qualification_answers``) was completed.
  You can use these SQL commands to compare *asterisk* and *xivo_stats* databases::

    select * from cel order by eventtime desc limit 1;
    select * from queue_log order by time desc limit 1;
    select * from callback_request order by reference_number desc limit 1;
    select * from qualification_answers order by time desc limit 1;


After Upgrade
=============

On XiVO PBX
-----------

* Debian system will be upgraded to Debian 9 (stretch)

  .. warning:: Please **read carefully** the :ref:`After the upgrade <debian_9_upgrade_notes_after>` section in the Debian 9 (stretch) upgrade notes.


* Recording: recording for queues was entirely modified

  * The ``xivocc-recording`` package will be automatically installed or upgraded with XiVO if you **don't** have ``xivo-gateway-recording``
    installed (to record on external server). These two packages are conflicting because they use the same dialplan subroutine names.
    If you are using gateway recording and your XiVO is capable to record internally, you should replace the old package by running
    ``apt-get install xivocc-recording``. Then follow the :ref:`recording_configuration` page. These features are available only with ``xivocc-recording``:

    * the :ref:`pause recording <agent_recording>` feature
    * and the :ref:`stop recording upon external transfer <stop_recording_upon_ext_xfer>` feature

    Gateway recording was also removed from documentation and it is not available in XiVO repository.

  * If you are upgrading from older version than 2017.03.02 with recording installed, you must follow the :ref:`upgrade_recording_xpbx`.
  * In the file :file:`/etc/asterisk/extensions_extra.d/xivocc-recording.conf` the ``ipbx_name`` was reset to its default value.
    You need to set it back to its previous value.
  * You MUST edit the queues configuration and (1) remove the subroutine used to start the recording,
    (2) and replace them by the correct configuration in the queue (see :ref:`recording_configuration`).

* Callbacks: callback API URL was changed. It is now prefixed with `configmgt`. If you have any AGI calling the callbacks API
  (for example `api/1.0/callback_lists`) you MUST add the prefix `configmgt` to it : `configmgt/api/1.0/callback_lists`.

* :ref:`https_certificate`: it is required to have ``subjectAltName`` defined in the HTTPS certificate.
  If you use the default HTTPS certificate, you must regenerate it. See :ref:`regenerating_certificate`.

* APT keyring lookup hashtable troubleshooting: this error can appear at the end of the upgrade before starting xivo services::

    gpg: lookup_hashtable failed: Unknown system error
    gpg: trustdb: searching trust record failed: Unknown system error
    gpg: Error: The trustdb is corrupted.
    gpg: You may try to re-create the trustdb using the commands:
    gpg:   cd ~/.gnupg
    gpg:   gpg --export-ownertrust > otrust.tmp
    gpg:   rm trustdb.gpg
    gpg:   gpg --import-ownertrust < otrust.tmp

  If this error appears, follow the printed procedure to recreate the lookup hashtable.


On XiVO CC
----------

Nothing specific, follow the :ref:`upgrade_cc` page.
