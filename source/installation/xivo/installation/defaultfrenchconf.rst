.. _default_french_conf:

################################
Default configuration for France
################################

.. note:: This option was introduced in 2017.01 version.


During the wizard you can choose to apply a default configuration for France : see :ref:`Wizard configuration step <wizard_configuration>`.
This option introduce a set of default parameters that will be useful particularly for a *XiVO PBX* installed in France.

The default parameters configured are listed in the sections below.


Default SIP parameters
**********************

In :menuselection:`Services -> IPBX -> General settings -> SIP Protocol` the following parameters are changed:

* for call presentation (in tab :menuselection:`Default`):

  * *Trust the Remote-Party-ID* is set to ``Yes``
  * *Send the Remote-Party-ID* is set to ``PAI``

* for codecs order (in tab :menuselection:`Signaling`): *G.711 A-law > G.722 > G.729A > H.264* is the default order.


Outgoing call rules
*******************

A set of default outgoing call rules according to the `French numbering plan <http://www.arcep.fr/?id=8146>`_ is set up by default.
In :menuselection:`Services -> IPBX -> Call management -> Outgoing calls` two outgoing call rules are defined:

#. *sortants-france*: pattern for french numbering plan numbers,
#. *urgences-france*: pattern for french emergency numbers.

.. note:: For these outgoing call rules, a 'void' cutomized trunk named 'Local/template_a_changer' is defined. This one must be deleted or modified according to your configuration.


Right call rules
****************

Also, a set of right call is predefined according to the set of outgoing call rules.
In :menuselection:`Services -> IPBX -> Call management -> Call permissions` you will find the following preconfigured right call
group:

+--------------------------+--------+-------------------------------------+
| Name [#]_                | Action | Description                         |
+==========================+========+=====================================+
| national                 | Allow  | Patterns for national numbers.      |
+--------------------------+--------+-------------------------------------+
| urgences                 | Allow  | Patterns for emergency numbers.     |
+--------------------------+--------+-------------------------------------+
| mobiles                  | Allow  | Patterns for mobile numbers.        |
+--------------------------+--------+-------------------------------------+
| numeros-a-valeur-ajoutee | Allow  | Patterns for services numbers.      |
+--------------------------+--------+-------------------------------------+
| international            | Allow  | Patterns for international numbers. |
+--------------------------+--------+-------------------------------------+
| refuser-tout             | Allow  | Patterns for all.                   |
+--------------------------+--------+-------------------------------------+


.. [#] this name can be used when importing users. See *Call permissions* section in :ref:`User import <user_import>`.


Default template device
***********************

The 'Default config device' template (in :menuselection:`Configuration -> Provisioning -> Template device`) has preconfigured language and time zone.