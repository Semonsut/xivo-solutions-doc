.. _switchboard_usage:

***********
Switchboard
***********

.. note::

  This section describes the Switchboard application features. It is available as a web application from your
  Web Browser.
  Currently it is not available as a desktop application.


**What is the Switchboard application ?**

Switchboard is a Web application for switchboard operators.
It is designed to handle the call flow of a company switchboard:

* see current incoming calls,
* answer incoming calls,
* easy search/transfer to user of the company
* being able to put calls in hold

All these actions can be used through keyboard shortcuts.

.. important::

  You need to follow the :ref:`switchboard configuration section <switchboard_configuration>` to be able to use the switchboard application.

.. contents::


Login
=====

The user can connect to the Switchboard application using https://YOUR_HOST/popc.


.. _switchboard_kbd_shortcuts:

Keyboard Navigation
===================

.. note:: The same shortcuts as the ones defined in CC Agent are available in the switchboard : see :ref:`CC Agent shortcuts <uc_agent_shortcuts>`

In addition, the switchboard allow you to navigate in the hold queue calls list using **Pageup** and **Pagedown** keys, and to retrieve a call by pressing **Enter**.

It also has two exclusive keybindings :

- **F8** to **complete a direct transfer** the current ongoing call to the phone number in the search bar

- **F9** to **send to hold queue** the ongoing call


Answer an incoming call
=======================

When the switchboard receives a call, the new call is added to the *Incoming Calls* list on the top right
and the phone starts ringing. The ccagent panel displays the client history tab of the person who is calling.

.. figure:: switchboard-incoming-call.png

The operator can answer this call by:

* clicking the *Answer* button on the left side, from the *CCagent* frame.
* Pressing the *F3* key.

Once the call has been answered, it is removed from the incoming calls list and only
displayed in the *CCagent* frame on the left side.


Hangup a Call
=============

The switchboard operator can hangup its current call by:

* clicking the *Hangup* button in the call control
* Pressing the *F4* key.


Handle Current Call
===================

Once the call has been answered and placed in the current call frame, the operator has 3 choices:

* transfer the call to another user
* put the call on hold by transferring it to the hold queue
* retrieve the call from the hold queue
* end the call using the *Hangup* button

Transferring a call
-------------------

When the switchboard operator has answered the call, they can transfer it by making either an attended transfer or a direct transfer to a user.

Attended transfer to a user
^^^^^^^^^^^^^^^^^^^^^^^^^^^

To make an attended transfer the operator can :

  * dial a number in the search bar and press Enter
  * call a number from the search result

Then complete the transfer by either:

  * clicking the *transfer button* from the ccagent call line
  * pressing the *F7* key

Direct transfer to a user
^^^^^^^^^^^^^^^^^^^^^^^^^

To make a direct transfer the operator can :

  * dial a number in the search bar and press the *F8* key
  * search for a user and click the Direct Transfer button from the search results


Putting a call on hold
----------------------

The switchboard operator can put a call on hold by :

* clicking the *Hold* button in the call control
* pressing the *F9* key

When placing the call on hold, it will be removed from the *CCagent* frame and displayed in the *Waiting calls* list on the bottom right.
The time counter shows how long the call has been waiting. The calls are ordered from the oldest to the newest.

In this example, the switchboard operator placed two calls on hold and can now answer the entering call that is currently ringing.

.. figure:: switchboard-calls-on-hold.png


Retrieving a call on hold
=========================

After a call was put on hold, the switchboard operator can retrieve it.

To retrieve a call on hold you need to click on the call you want to retrieve.

Note that you can chose which call you want to retrieve (i.e. you can retrieve the third one before the first one).


Switchboard chat
================

The switchboard operator can use the chat to start a conversation with an UC Assistant user or another switchboard operator.
For more information about available chat features you can refer to :ref:`uc-assistant_chat`

.. figure:: switchboard-chat.png
