.. _ccmanager_configuration:

**********
CC Manager
**********

See :ref:`CC Manager features <ccmanager>`.

.. _ccmanager-security:

Access authorizations in CCManager
==================================

.. note:: Behavior was changed in 2017.LTS1 (see 2017.LTS1 release notes in :ref:`xivosolutions_release`)

By default, CCManager access is authorized only for users with *Administrateur* or *Superviseur* rigths (as defined in the
Configuration Management server).
If required, you can authorize all users to connect to the CCManager interface by setting the ``ENFORCE_MANAGER_SECURITY`` environment variable to ``false`` in the
:file:`/etc/docker/compose/custom.env` file::

   ...
   ENFORCE_MANAGER_SECURITY=false

Then you need to recreate the xucserver container with ``xivocc-dcomp up -d xuc``.
Then each user will be able to log in the CCManager. Otherwise, each user that wants to connect to the CCManager will need to have a *Administrateur* or *Superviseur* profile in the Configuration Management server.

