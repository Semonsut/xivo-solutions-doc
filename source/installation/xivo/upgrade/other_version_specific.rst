.. _other_version_proc:

*********************************
Other Version Specific Procedures
*********************************

Several version specific procedure are listed here.

Upgrading from XiVO 14.11 and before
====================================

When upgrading from XiVO 14.11 or earlier, you must do the following, before the normal upgrade::

   sed -i 's/xivo\.fr/xivo.solutions/g' /etc/apt/sources.list.d/*.list


Upgrading from XiVO 14.01, 14.02, 14.03, 14.04 installed from the ISO
=====================================================================

In those versions, xivo-upgrade keeps XiVO on the same version. You must do the following, before
the normal upgrade::

   echo "deb http://mirror.xivo.solutions/debian/ xivo-five main" > /etc/apt/sources.list.d/xivo-upgrade.list \
   && apt-get update \
   && apt-get install xivo-fai \
   && rm /etc/apt/sources.list.d/xivo-upgrade.list \
   && apt-get update


Upgrading from XiVO 13.24 and before
====================================

When upgrading from XiVO 13.24 or earlier, you must do the following, before the normal upgrade:

#. Ensure that the file :file:`/etc/apt/sources.list` is *not* configured on ``archive.debian.org``.
   Instead, it must be configured with a non-archive mirror, but still on the ``squeeze``
   distribution, even if it is not present on this mirror. For example::

    deb http://ftp.us.debian.org/debian squeeze main

#. Add ``archive.debian.org`` in another file::

    cat > /etc/apt/sources.list.d/squeeze-archive.list <<EOF
    deb http://archive.debian.org/debian/ squeeze main
    EOF

And after the upgrade::

   rm /etc/apt/sources.list.d/squeeze-archive.list


Upgrading from XiVO 13.03 and before
====================================

When upgrading from XiVO 13.03 or earlier, you must do the following, before the normal upgrade::

   wget http://mirror.xivo.solutions/xivo_current.key -O - | apt-key add -


Upgrading from XiVO 12.13 and before
====================================

When upgrading from XiVO 12.13 or earlier, you must do the following, before the normal upgrade::

   apt-get update
   apt-get install debian-archive-keyring


Upgrading from XiVO 1.2.1 and before
====================================

Upgrading from 1.2.0 or 1.2.1 requires a special procedure before executing ``xivo-upgrade``::

   apt-get update
   apt-get install xivo-upgrade
   /usr/bin/xivo-upgrade


