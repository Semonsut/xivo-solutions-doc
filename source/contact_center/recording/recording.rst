.. _recording:

*********
Recording
*********

*XiVO CC* includes a call recording feature:

* recording is done on *XiVO PBX* (or the XiVO Gateway)
* and recorded files are sent to the *XiVO CC* Recording server.

It's then possible to search for recordings and download recorded files from *Recording server*.

If connected user is an administrator, he will also be able to download an access log file that tracks all actions made on files by all users having access to recording server.

.. contents:: :local:

Recording Server
================

Recording must first be enabled on the *XiVO PBX*, see :ref:`recording_configuration` (or on the *XiVO Gateway*, see :ref:`recording_gw_configuration`).

Description
-----------

When recording is enabled the whole conversation between caller and callee is recorded and then sent on dedicated recording server. All the files are then available for download only for predefined *granted* users (see :ref:`profile_mgt-profile`).

.. _recording_search_listen:

Search, Download, Listen
------------------------

Here's the recording search page:

.. figure:: search.png
   :scale: 100%

You can then do following actions:

#. Find by caller, callee, agent number etc.
#. Find by callid
#. Listen or download the recording
#. Download access log to recording files

.. _recording_disk_space:

Disk Space
----------

On *Recording server*, one can monitor the space used by the audio files stored in ``Contrôle d'enregistrement`` menu.

.. figure:: store.png
   :scale: 100%

.. _recording_access:

Access logs
-----------

Any action made from the UI on a recording file (``result``, ``listen`` or ``download``) is tracked in access log that can be downloaded by clicking on following icon if you have administrator role.

.. figure:: access.png
   :scale: 100%

.. note:: This access log is defined to track information for 6 months by default.


File Sync
---------

Recording is done on *XiVO PBX* and sent to *Recording server*. If recorded file can't be synchronized on *XiVO CC* Recording Server, files might be found on *XiVO PBX* in

.. code-block:: bash

    ls -al /var/spool/xivocc-recording/failed

These files will be automatically resynchronized from *XiVO PBX* to *XiVO CC* Recording server each night.


Recording and Transfers
=======================

In case an agent has transferred a call to another queue, which was answered by the agent available in that queue, the recording feature will display both agents (name and number) in column **Agent**
and both queues (name and number) under column **File** in one recording.

.. figure:: search-transfer.png
   :scale: 100%

.. _automatic_stop_start_rec_on_queues:

Automatic Stop/Start Recording On Queues
========================================

When a call enters a queue, the recording will be started (or not) according to the queue Recording mode (see :ref:`queue_recording_configuration`).
If this call is transferred to another queue, it will stop or start the recording according to the following table:

+-----------------------+---------------------------------------------------------+
| **Action**            | *Recording mode of 'transferred to' Queue*              |
+ **on**                +-------------------+--------------------+----------------+
| **Recording**         | *Recorded*        |*Recorded On Demand*| *Not Recorded* |
+------------+----------+-------------------+--------------------+----------------+
|            |*Active*  |                   |                    | Stop recording |
+*Current*   +----------+-------------------+--------------------+----------------+
|*Call*      |*Paused*  | Unpause recording |                    | Stop recording |
+*Recording* +----------+-------------------+--------------------+----------------+
|*State*     |*Disabled*| Stop recording    | Stop recording     |                |
+------------+----------+-------------------+--------------------+----------------+

This behavior can be deactivcated, see :ref:`configuration section <automatic_stop_start_rec_on_queues_conf>`

.. _stop_recording_upon_ext_xfer:

Automatic Stop Recording Upon External Transfer
===============================================

Recording is stopped when both parties of the call are external.

For example if an external incoming call is recorded and if, at some point, it is transferred to an external party,
as soon as the transfer is completed, the recording of the incoming call will be stopped.

In the same way if an internal user makes an outgoing call which is recorded and then transfers it to another external party,
as soon as the transfer is completed, the recording of the outgoing call will be stopped.

This behavior can be deactivated, see :ref:`configuration section <stop_recording_upon_ext_xfer_conf>`.


.. _recording_filtering:

Recording filtering
===================

To configure this feature, see :ref:`recording_filtering_configuration`.

Description
-----------
*Recording server* allows to prevent some numbers not to be recorded.

You can deactivate recording either

* for specific called numbers (called Incalls or called Queues or called Users),
* or, on outgoing calls, for calling Users internal numbers

.. note:: This feature is designed to activate recording globally (e.g. for every Queues) and then deactivate it for some Queues
          (e.g. for queue 1001)

To do so, navigate to : http://XIVOCC_IP:9400/recording_control and in page ``Contrôle d'enregistrement`` you can add or remove the
number to disable recording on this number.

.. figure:: stop-record.png
   :scale: 100%


Limitations
-----------

* Recording can only be disabled for specific Incalls, Queues, Users and External called numbers.
* Recording can't be disabled for one Agent.
* Recording can only be disabled by the object number (to disable recording for one queue it must have a number).

